import {LOGIN_USER, METAMASK_CONNECTION_CHANGE, REQUEST_FAILURE, SET_FORM_ALERT, SET_USER_INFO} from './reducer'
import {push} from 'react-router-redux'
import request from 'axios';
import AuthService from "../../services/AuthService";

export const setFormAlert = (formAlert) => async (dispatch) => {
    dispatch({
        type: SET_FORM_ALERT,
        payload: {
            message: formAlert.message,
            failure: formAlert.status,
        }
    })
}

export const setUserInfo = (userData) => async (dispatch) => {
    dispatch({
        type: SET_USER_INFO,
        payload: {
            earthResource: userData.earthResource,
            email: userData.email,
            fireResource: userData.fireResource,
            firstname: userData.firstname,
            lastname: userData.lastname,
            printed: userData.printed,
            tokenAmount: userData.tokenAmount,
            username: userData.username,
            walletAddress: userData.walletAddress,
            walletTokenAmount: userData.walletTokenAmount,
            waterResource: userData.waterResource,
            _id: userData._id,
        }
    })
}


export const setMetamask = (metamaskConnectionBody) => async (dispatch) => {
    dispatch({
        type: METAMASK_CONNECTION_CHANGE,
        payload: {
            metamaskConnection: metamaskConnectionBody.metamaskConnection,
            metamaskChosenAddress: metamaskConnectionBody.metamaskChosenAddress,
        }
    })
}

export const registerUserAction = (newUserBody) => async (dispatch) => {
    const response = await AuthService.register(newUserBody);

    const {success, message} = response.data;

    if (success) {
        dispatch(push("/"));
        alert("success. Please login with your credentials")
    } else {
        dispatch({
            type: REQUEST_FAILURE,
            payload: {
                failure: true,
                message
            }
        })
    }
}

export const loginUserAction = ({email, password}) =>
    async (dispatch) => {
        const response = await AuthService.login(email, password);

        const {success, message, userData} = response.data

        if (success) {
            localStorage.setItem('token', userData.accessToken);
            localStorage.setItem('email', email);

            await dispatch({
                type: LOGIN_USER,
                payload: {
                    userData: userData.user,
                    token: userData.accessToken
                }
            })
        } else {
            dispatch({
                type: REQUEST_FAILURE,
                payload: {
                    failure: true,
                    message
                }
            })
        }
    };