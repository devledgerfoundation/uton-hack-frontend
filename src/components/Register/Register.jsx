import React from 'react';
import './Register.css';
import {Link} from 'react-router-dom'
import FormAlert from "../FormAlert/FormAlert";
import {registerUserAction} from "../../store/user/action";
import {useDispatch} from "react-redux";

function FormInput({onChangeInput, label, placeholder, name, type}) {
    return (
        <div className="mb-3">
            <div className="form-group">
                <label>{label}</label>
                <input
                    onChange={onChangeInput}
                    type={type}
                    name={name}
                    className="form-control"
                    placeholder={placeholder}
                    required
                />
            </div>
        </div>
    )
}

export default function Register() {
    const dispatch = useDispatch();

    const [formState, setFormState] = React.useState({
        firstnameInput: '',
        lastnameInput: '',
        usernameInput: '',
        emailInput: '',
        passwordInput: '',
        passwordRepeatInput: '',
        rememberStatusInput: false,
        walletAddress: ''
    })

    const onChangeInput = (e) => {
        setFormState({
            ...formState,
            [e.target.name]: e.target.value,
        })
    }

    const generateWalletAddress = (length) => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }

    const handleSubmit = async e => {
        e.preventDefault();

        await dispatch(registerUserAction({
            firstname: formState.firstnameInput,
            lastname: formState.lastnameInput,
            username: formState.usernameInput,
            email: formState.emailInput,
            password: formState.passwordInput,
            password_repeat: formState.passwordRepeatInput,
            walletAddress: generateWalletAddress(28),
        }))
    }

    return (
        <div>
            <form className="mb-3" onSubmit={handleSubmit}>
                <h3>Register</h3>
                <FormInput
                    label="First Name"
                    onChangeInput={onChangeInput}
                    placeholder={"Enter firstname"}
                    name={"firstnameInput"}
                    type={"text"}
                />
                <FormInput
                    label="Last Name"
                    onChangeInput={onChangeInput}
                    placeholder={"Last Name"}
                    name={"lastnameInput"}
                    type={"text"}
                />
                <FormInput
                    label="Username"
                    onChangeInput={onChangeInput}
                    type={"text"}
                    name={"usernameInput"}
                    placeholder={"Enter username"}
                />
                <FormInput
                    label="Email address"
                    onChangeInput={onChangeInput}
                    type={"email"}
                    name={"emailInput"}
                    placeholder={"Enter email"}
                />
                <FormInput
                    label="Password"
                    onChangeInput={onChangeInput}
                    type={"password"}
                    name={"passwordInput"}
                    placeholder={"Enter password"}
                />
                <FormInput
                    label="Repeat your password"
                    onChangeInput={onChangeInput}
                    type={"password"}
                    name={"passwordRepeatInput"}
                    placeholder={"Re-enter password"}
                />

                <button
                    type="submit"
                    className="btn btn-primary btn-block mb-3"
                >
                    Submit
                </button>

                <FormAlert/>
            </form>
            <Link to={"/"}>
                Login
            </Link>
        </div>
    );
}