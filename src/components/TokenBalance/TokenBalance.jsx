import React from 'react';
import './TokenBalance.css';
import {Link} from "react-router-dom";
import Web3 from "web3";
import BigNumber from "bignumber.js";
import Loader from "react-loader-spinner";
import FormAlert from "../FormAlert/FormAlert";
import {useDispatch, useSelector} from "react-redux";
import {setFormAlert, setUserInfo} from "../../store/user/action";
import userService from '../../services/UserService';

const tknAbi = require('../../static/GLRY.json');

export default function TokenBalance() {
    const [depositInput, setDepositInput] = React.useState('')
    const [withdrawInput, setWithdrawInput] = React.useState('')
    const [loading, setLoading] = React.useState(false);

    React.useEffect(() => {
        userService.getUserById(_id).then((res) => {
            console.log(res.data)
            dispatch(setUserInfo(res.data))
        })
    }, [])

    const {
        _id,
        walletAddress,
        tokenAmount,
        walletTokenAmount,
    } = useSelector((state) => state.user);

    const dispatch = useDispatch();

    const onWithdrawChangeInput = (e) => {
        e.preventDefault()
        setWithdrawInput((_) => e.target.value)
    }

    const onDepositChangeInput = (e) => {
        e.preventDefault()
        setDepositInput((_) => e.target.value)
    }

    const withdraw = async () => {
        try {
            let withdrawNumber = parseInt(withdrawInput, 10);
            if (walletAddress && withdrawNumber <= tokenAmount) {
                console.log(' user.walletAddress: ', walletAddress)
                setLoading((_) => true)
                await userService.updateUserTokenAmount(_id, (tokenAmount - withdrawNumber), (walletTokenAmount + withdrawNumber)).then(res => {
                    console.log('Withdraw token amount from user balance promise then: ', res.data)
                })
                dispatch(setFormAlert({
                    message: '',
                    status: false,
                }))
                window.location.reload()

                setLoading((_) => false)
            } else {
                if (withdrawNumber > tokenAmount) {
                    dispatch(setFormAlert({
                        message: 'Withdraw should be less or equal than your In-Game balance',
                        status: true,
                    }))
                }
            }
        } catch (e) {
            console.log("e: ", e)
            console.log("e.data: ", e.data)
        }
    }

    const deposit = async () => {
        try {
            let depositNumber = parseInt(depositInput, 10);
            if (walletAddress && depositNumber <= walletTokenAmount) {
                console.log(' user.walletAddress: ', walletAddress)
                setLoading((_) => true)
                await userService.updateUserTokenAmount(_id, (depositNumber + tokenAmount), (walletTokenAmount - depositNumber)).then(res => {
                    console.log('Deposit token amount from user balance promise then: ', res.data)
                })
                dispatch(setFormAlert({
                    message: '',
                    status: false,
                }))
                window.location.reload()

                setLoading((_) => false)
            } else if (depositNumber > walletTokenAmount) {
                dispatch(setFormAlert({
                    message: 'Deposit should be less or equal than your On-Chain balance',
                    status: true,
                }))
            }
        } catch (e) {
            console.log("e: ", e)
            console.log("e.data: ", e.data)
        }
    }


    if (loading) {
        return (
            <div className="text-center">
                <Loader
                    type="Puff"
                    color="#00BFFF"
                    height={100}
                    width={100}
                />
            </div>
        );
    } else {
        return (
            <div className="token_balance__container">
                <h5 className="card-title">Total Token Balance</h5>
                <ul className="list-group list-group-flush mb-3">
                    <li className="list-group-item"></li>
                    <li className="list-group-item">On-Chain Balance: {walletTokenAmount} GLRY</li>
                    <li className="list-group-item">In-Game Balance: {tokenAmount} GLRY</li>
                    <li className="list-group-item"></li>
                </ul>
                <div className="token_balance__inputs m-4">
                    <div>
                        <div>
                            <h6 className="mb-0">Deposit</h6>
                            <label className="token_balance__input_label">Put tokens into the game</label>
                        </div>
                        <input
                            id="userDeposit"
                            name="depositInput"
                            type="text"
                            className="token_balance__input"
                            pattern="[0-9]*"
                            onChange={onDepositChangeInput}
                            required
                        />
                        <button
                            id="btnDeposit"
                            onClick={deposit}
                            className="token_balance__button"
                        >
                            Deposit
                        </button>
                    </div>
                    <div>
                        <div>
                            <h6 className="mb-0">Withdraw</h6>
                            <label className="token_balance__input_label">Get tokens from the game</label>
                        </div>
                        <input
                            id="userWithdraw"
                            name="withdrawInput"
                            className="token_balance__input"
                            type="text"
                            pattern="[0-9]*"
                            onChange={onWithdrawChangeInput}
                            required
                        />
                        <button
                            id="btnWithdraw"
                            className="token_balance__button"
                            onClick={withdraw}
                        >
                            Withdraw
                        </button>
                    </div>
                </div>
                <FormAlert/>
                <div className="token_mint__navigation">
                    <Link to={"./"}>
                        Home
                    </Link>
                    <Link to={"./nftList"}>
                        See all my NFTs
                    </Link>
                </div>
            </div>
        );
    }
}

