import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import rootReducer, {RootInitialState} from './rootReducer';
import {composeWithDevTools} from 'redux-devtools-extension';
import {createStateSyncMiddleware, initStateWithPrevTab} from 'redux-state-sync'

const initialState = {
    ...RootInitialState
};

const middleware = [thunk, createStateSyncMiddleware({})];

const store = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middleware))
);

initStateWithPrevTab(store)

export default store;
