import $api from '../http/index';

export default class NftTokenService {
    static async getAllNftsByUserEmail(email) {
        return $api.get(`/nft_token/getAllNftsByUserEmail/${email}`)
    }

    static async create(newTokenBody) {
        console.log('printing nft')
        return $api.post('/nft_token/create', newTokenBody)
    }
}