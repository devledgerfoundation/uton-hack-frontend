import React from 'react';
import './TokenMint.css';
import axios from 'axios';
import Loader from "react-loader-spinner";
import Web3 from 'web3';
import nftAbi from '../../static/InfiniteHeroAbi.json'
import {Link} from "react-router-dom";
import FormAlert from "../FormAlert/FormAlert";
import {useDispatch, useSelector} from "react-redux";
import {setFormAlert, setMetamask} from "../../store/user/action";
import {Alert} from "@mui/material";
import modelService from '../../services/ModelService';
import nftTokenService from '../../services/NftTokenService';

export default function TokenMint() {
    const {
        _id,
        failure,
        walletAddress,
    } = useSelector((state) => state.user);

    const dispatch = useDispatch();

    const [tokenInfo, setTokenInfo] = React.useState({
        modelId: '',
        description: '',
        name: '',
        walletAddress: '',
        imageUrl: '',
        printed: false,
    })

    const [loading, setLoading] = React.useState(false);
    const [metamaskPrint, setMetamaskPrint] = React.useState(false);

    React.useEffect(async () => {
        dispatch(setFormAlert({
            message: '',
            status: false,
        }))
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        console.log(params)

        await modelService.getById(params.modelId)
            .then(res => {
                if (!res.data.success) {
                    dispatch(setMetamask({
                        message: res.data.message,
                        status: true
                    }))
                } else {
                    setTokenInfo({
                            name: res.data.model.name,
                            description: res.data.model.description,
                            walletAddress: res.data.model.user.walletAddress,
                            modelId: params.modelId,
                            imageUrl: res.data.model.base_image,
                            nftImageUrl: res.data.model.nft_image,
                            printed: res.data.model.printed,
                        }
                    )
                }
            })
            .catch((err) => {
                console.log(err)
                console.log(err.data)
                console.log(err.message)
                dispatch(setFormAlert({
                    message: err.message,
                    status: true
                }))
            })
    }, [])

    const generate_token_id = (length) => {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }

    const mintToken = async (e) => {
        await modelService.updateModelPrintedStatus(tokenInfo.modelId, true).then(res => {
            console.log("updateModelPrintedStatus: done")
        })
        setLoading((_) => true)

        await nftTokenService.create({
            name: tokenInfo.name,
            model_id: tokenInfo.modelId,
            user_id: _id,
            description: tokenInfo.description,
            image_uri: tokenInfo.imageUrl,
            token_id: generate_token_id(28)
        }).then(res => {
            console.log("nft_token creation: done")
            console.log("nft_token data: ", res.data)
        })

        setLoading((_) => false)
        setMetamaskPrint((_) => true)
    }

    if (loading) {
        return (
            <div className="text-center">
                <Loader
                    type="Puff"
                    color="#00BFFF"
                    height={100}
                    width={100}
                />
            </div>
        );
    } else {
        return (
            <div>
                <h5 className="card-title">Token Information</h5>
                <ul className="list-group list-group-flush mb-3">
                    <li className="list-group-item">Wallet address: {walletAddress}</li>
                    <li className="list-group-item">Token Name: {tokenInfo.name}</li>
                    <li className="list-group-item">Token Description: {tokenInfo.description}</li>
                </ul>
                {
                    tokenInfo.imageUrl === ''
                        ?
                        <div className="btn__mint mb-3">
                            <Alert severity="error">
                                No token image found
                            </Alert>
                        </div>
                        :
                        <div className="btn__mint mb-3">
                            <img className="mb-3"
                                 src={tokenInfo.imageUrl}
                                 alt="Card image cap"/>
                        </div>

                }
                <div className="btn__mint mb-3">
                    <button
                        type="button"
                        disabled={failure || tokenInfo.printed}
                        className="btn btn-primary"
                        onClick={mintToken}
                    >
                        Mint NFT
                    </button>
                </div>
                <FormAlert/>
                {
                    tokenInfo.printed
                        ?
                        <Alert severity="error" className="mb-3">
                            Token already printed. You can't print it for the second time
                        </Alert>
                        :
                        <></>
                }
                {
                    metamaskPrint ?
                        <Alert severity="success" className="mb-3">
                            Your token successfully printed.
                        </Alert>
                        :
                        <></>
                }
                <div className="token_mint__navigation">
                    <Link to={"./"}>
                        Home
                    </Link>
                    <Link to={"./nftList"}>
                        See all my NFTs
                    </Link>
                </div>
            </div>
        )
    }
}