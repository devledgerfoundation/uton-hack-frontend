export const initialStateUser = {};

export const LOGIN_USER = 'LOGIN_USER';
export const REQUEST_FAILURE = 'REQUEST_FAILURE';
export const METAMASK_CONNECTION_CHANGE = 'METAMASK_CONNECTION_CHANGE';
export const SET_FORM_ALERT = 'SET_FORM_ALERT';
export const SET_USER_INFO = 'SET_USER_INFO';

export default function reducer(state = initialStateUser, action) {
    switch (action.type) {
        case LOGIN_USER:
            return {
                ...state,
                ...action.payload.userData,
                token: action.payload.token
            }
        case REQUEST_FAILURE:
            return {
                ...state,
                user: action.payload.user,
                failure: action.payload.failure,
                message: action.payload.message,
            }
        case METAMASK_CONNECTION_CHANGE:
            return {
                ...state,
                user: action.payload.user,
                metamaskConnection: action.payload.metamaskConnection,
                metamaskChosenAddress: action.payload.metamaskChosenAddress,
            }
        case SET_USER_INFO:
            return {
                ...state,
                ...action.payload,
            }
        case SET_FORM_ALERT:
            return {
                ...state,
                failure: action.payload.failure,
                message: action.payload.message,
            }
        default:
            return state
    }
}
