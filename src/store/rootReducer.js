import {combineReducers} from 'redux';
import user from './user';
import { initialStateUser } from './user/reducer';
import { withReduxStateSync } from 'redux-state-sync'

export const RootInitialState = {
    user: initialStateUser
}

export default withReduxStateSync(combineReducers({
    user: user.reducer,
}));
